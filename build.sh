#!/bin/sh

# Copyright 2023 Marek Buras
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

set -e

CWD=$(pwd)

PY_MAJ_VER=3.9
PYTHON_VERSION="${PY_MAJ_VER}.18"

# Directories used during the build process
SRC_DIR="${HOME}/Developer/src"
BUILD_DIR="${HOME}/Developer/_build"

# Framework destination directories
USER_FRAMEWORK_DIR="${HOME}/Library/Frameworks"
PYTHON_FRAMEWORK_DIR="${USER_FRAMEWORK_DIR}/Python.framework/Versions/${PY_MAJ_VER}"

# OpenSSL
SSL_DIR=${BUILD_DIR}/libs/openssl
OPENSSL_VERSION=1.1.1w

# LZMA
XZ_VERSION=5.4.5
XZ_DIR="${BUILD_DIR}/libs/xz"
XZ_LDFLAGS="-L${XZ_DIR}/lib"
XZ_CFLAGS="-I${XZ_DIR}/include"

# TCL/TK
TCL_VERSION=8.6.13
TK_VERSION=8.6.13
TCLTK_DIR="${BUILD_DIR}/libs/tcltk"
TCLTK_CFLAGS="-I${TCLTK_DIR}/include"
TCLTK_LDFLAGS="-L${TCLTK_DIR}/lib -ltcl8.6 -ltk8.6"

# SQLite
SQLITE_VERSION=3440200
SQLITE_DIR="${BUILD_DIR}/libs/sqlite"
SQLITE_CFLAGS="-I${SQLITE_DIR}/include"
SQLITE_LDFLAGS="-L${SQLITE_DIR}/lib"


if [ ! -f "${CWD}/CKSUM" ]; then
	echo "Missing CKSUM file in the current directory."
	exit 1
fi

##############################################################################
# Download source files
##############################################################################

mkdir -p "${SRC_DIR}" || exit 1
pushd "${SRC_DIR}"
for url in $(cat "${CWD}/sources.txt") ; do
	filename=$(basename $url)
	if [ ! -f "${SRC_DIR}/$filename" ]; then
		echo "Downloading $filename from $url"
		curl -L $url -o "${SRC_DIR}/$(basename $url)"
	fi
done
popd || exit 1

pushd "${SRC_DIR}" || exit 1
echo "Checking source files integrity."
shasum -a 256 -c "${CWD}/CKSUM" || exit 1
popd

mkdir -p "${BUILD_DIR}" || exit 1

##############################################################################
# Build and install OpenSSL into temporary location
##############################################################################

echo "Building OpenSSL ${OPENSSL_VERSION}"
[ -d "${BUILD_DIR}/openssl-${OPENSSL_VERSION}" ] && rm -rf "${BUILD_DIR}/openssl-${OPENSSL_VERSION}"
tar xf "${SRC_DIR}/openssl-${OPENSSL_VERSION}.tar.gz" -C "${BUILD_DIR}" || exit 1

pushd "${BUILD_DIR}/openssl-${OPENSSL_VERSION}" || exit 1
./Configure \
	darwin64-$(uname -m)-cc \
	enable-ec_nistp_64_gcc_128 \
	no-idea \
	no-mdc2 \
	no-rc5 \
	no-zlib \
	no-ssl3 \
	shared \
	--prefix="${SSL_DIR}" || exit 1
make depend || exit 1
make all || exit 1
make test || exit 1
make install_sw || exit 1
popd || exit 1

##############################################################################
# Build and install xz/lzma into temporary location
##############################################################################

echo "Building xz ${XZ_VERSION}"
[ -d "${BUILD_DIR}/xz-${XZ_VERSION}" ] && rm -rf "${BUILD_DIR}/xz-${XZ_VERSION}"
tar xf "${SRC_DIR}/xz-${XZ_VERSION}.tar.gz" -C "${BUILD_DIR}" || exit 1
pushd "${BUILD_DIR}/xz-${XZ_VERSION}" || exit 1
./configure --prefix="${XZ_DIR}" --disable-dependency-tracking || exit 1
make || exit 1
make install || exit 1
popd || exit 1

##############################################################################
# Build and install Tcl/Tk into temporary location
##############################################################################

echo "Building tcl ${TCL_VERSION}"
[ -d "${BUILD_DIR}/tcl${TCL_VERSION}" ] && rm -rf "${BUILD_DIR}/tcl${TCL_VERSION}"
tar xf "${SRC_DIR}/tcl${TCL_VERSION}-src.tar.gz" -C "${BUILD_DIR}" || exit 1
pushd "${BUILD_DIR}/tcl${TCL_VERSION}/unix" || exit 1
./configure --prefix="${TCLTK_DIR}" \
	--enable-shared \
	--enable-threads \
	|| exit 1
make || exit 1
make install || exit 1
popd || exit 1

echo "Building tk ${TK_VERSION}"
[ -d "${BUILD_DIR}/tk${TK_VERSION}" ] && rm -rf "${BUILD_DIR}/tk${TK_VERSION}"
tar xf "${SRC_DIR}/tk${TK_VERSION}-src.tar.gz" -C "${BUILD_DIR}" || exit 1
pushd "${BUILD_DIR}/tk${TK_VERSION}/unix" || exit 1
./configure --prefix="${TCLTK_DIR}" \
	--enable-aqua \
	--enable-shared \
	--enable-threads \
	|| exit 1
make || exit 1
make install || exit 1
popd || exit 1

##############################################################################
# Build and install SQLite into temporary location
##############################################################################

echo "Building sqlite ${SQLITE_VERSION}"
[ -d "${BUILD_DIR}/sqlite-autoconf-${SQLITE_VERSION}" ] && rm -rf "${BUILD_DIR}/sqlite-autoconf-${SQLITE_VERSION}"
tar xf "${SRC_DIR}/sqlite-autoconf-${SQLITE_VERSION}.tar.gz" -C "${BUILD_DIR}" || exit 1
pushd "${BUILD_DIR}/sqlite-autoconf-${SQLITE_VERSION}" || exit 1
./configure \
 --prefix="${SQLITE_DIR}" \
 --enable-threadsafe \
 --enable-shared=no \
 --enable-static=yes \
 --disable-readline \
 --disable-dependency-tracking \
 CFLAGS="-Os -DSQLITE_ENABLE_FTS5 -DSQLITE_ENABLE_FTS4 -DSQLITE_ENABLE_FTS3_PARENTHESIS -DSQLITE_ENABLE_JSON1 -DSQLITE_ENABLE_RTREE -DSQLITE_TCL=0" || exit 1
make || exit 1
make install || exit 1
popd || exit 1

##############################################################################
# Build and install Python into Framework directory in $HOME
##############################################################################

echo "Building Python ${PYTHON_VERSION}..."
[ -d "${BUILD_DIR}/Python-${PYTHON_VERSION}" ] && rm -rf "${BUILD_DIR}/Python-${PYTHON_VERSION}"
tar xf "${SRC_DIR}/Python-${PYTHON_VERSION}.tgz" -C "${BUILD_DIR}" || exit 1

pushd "${BUILD_DIR}/Python-${PYTHON_VERSION}" || exit 1
./configure -C --enable-framework="${USER_FRAMEWORK_DIR}" \
	--with-openssl="${SSL_DIR}" \
	--without-ensurepip \
	--with-computed-gotos \
	--enable-optimizations \
	--with-tcltk-includes="${TCLTK_CFLAGS}" \
	--with-tcltk-libs="${TCLTK_LDFLAGS}" \
	LDFLAGS="${XZ_LDFLAGS} ${SQLITE_LDFLAGS}" \
	CFLAGS="${XZ_CFLAGS} ${SQLITE_CFLAGS}" \
    || exit 1
make || exit 1
make install || exit 1
make frameworkinstallextras || exit 1
popd || exit 1


# Copy requires shared libraries into the framework directory
cp -R ${BUILD_DIR}/libs/*/lib/*dylib \
	"${PYTHON_FRAMEWORK_DIR}/lib/" || exit 1

# Copy tcl/tk libraries into the framework directory (required for tkinter)
cp -R ${BUILD_DIR}/libs/tcltk/lib/{tcl,tk}8.6 \
	"${PYTHON_FRAMEWORK_DIR}/lib/" || exit 1

echo "Updating install_name of Python modules to ${PYTHON_FRAMEWORK_DIR}"
install_name_tool -change {${BUILD_DIR}/libs/xz,${PYTHON_FRAMEWORK_DIR}}/lib/liblzma.5.dylib ${PYTHON_FRAMEWORK_DIR}/lib/python3.9/lib-dynload/_lzma.cpython-39-darwin.so || exit 1
install_name_tool -change {${BUILD_DIR}/libs/openssl,${PYTHON_FRAMEWORK_DIR}}/lib/libssl.1.1.dylib ${PYTHON_FRAMEWORK_DIR}/lib/python3.9/lib-dynload/_hashlib.cpython-39-darwin.so || exit 1
install_name_tool -change {${BUILD_DIR}/libs/openssl,${PYTHON_FRAMEWORK_DIR}}/lib/libcrypto.1.1.dylib ${PYTHON_FRAMEWORK_DIR}/lib/python3.9/lib-dynload/_hashlib.cpython-39-darwin.so || exit 1
install_name_tool -change {${BUILD_DIR}/libs/openssl,${PYTHON_FRAMEWORK_DIR}}/lib/libssl.1.1.dylib ${PYTHON_FRAMEWORK_DIR}/lib/python3.9/lib-dynload/_ssl.cpython-39-darwin.so || exit 1
install_name_tool -change {${BUILD_DIR}/libs/openssl,${PYTHON_FRAMEWORK_DIR}}/lib/libcrypto.1.1.dylib ${PYTHON_FRAMEWORK_DIR}/lib/python3.9/lib-dynload/_ssl.cpython-39-darwin.so || exit 1
install_name_tool -change {${BUILD_DIR}/libs/tcltk,${PYTHON_FRAMEWORK_DIR}}/lib/libtcl8.6.dylib ${PYTHON_FRAMEWORK_DIR}/lib/python3.9/lib-dynload/_tkinter.cpython-39-darwin.so || exit 1
install_name_tool -change {${BUILD_DIR}/libs/tcltk,${PYTHON_FRAMEWORK_DIR}}/lib/libtk8.6.dylib ${PYTHON_FRAMEWORK_DIR}/lib/python3.9/lib-dynload/_tkinter.cpython-39-darwin.so || exit 1

# Compile Python library
echo "Compiling Python standard library..."
"${PYTHON_FRAMEWORK_DIR}/bin/python${PY_MAJ_VER}" -E -s -Wi \
    "${PYTHON_FRAMEWORK_DIR}/lib/python${PY_MAJ_VER}/compileall.py" -q -j0 \
    -f -x 'bad_coding|badsyntax|site-packages|lib2to3/tests/data' \
    "${PYTHON_FRAMEWORK_DIR}/lib/python${PY_MAJ_VER}"

"${PYTHON_FRAMEWORK_DIR}/bin/python${PY_MAJ_VER}" -E -s -Wi -O \
    "${PYTHON_FRAMEWORK_DIR}/lib/python${PY_MAJ_VER}/compileall.py" -q -j0 \
    -f -x 'bad_coding|badsyntax|site-packages|lib2to3/tests/data' \
    "${PYTHON_FRAMEWORK_DIR}/lib/python${PY_MAJ_VER}"

"${PYTHON_FRAMEWORK_DIR}/bin/python${PY_MAJ_VER}" -E -s -Wi \
    "${PYTHON_FRAMEWORK_DIR}/lib/python${PY_MAJ_VER}/compileall.py" -q -j0 \
    -f -x badsyntax \
    "${PYTHON_FRAMEWORK_DIR}/lib/python${PY_MAJ_VER}/site-packages"

"${PYTHON_FRAMEWORK_DIR}/bin/python${PY_MAJ_VER}" -E -s -Wi -O \
    "${PYTHON_FRAMEWORK_DIR}/lib/python${PY_MAJ_VER}/compileall.py" -q -j0 \
    -f -x badsyntax \
    "${PYTHON_FRAMEWORK_DIR}/lib/python${PY_MAJ_VER}/site-packages"

# Cleanup
echo "Cleaning up..."
[ -d "${BUILD_DIR}" ] && rm -rf "${BUILD_DIR}"
echo "Build finished."