# MacPython 3.9 Builder

Build script for MacPython 3.9.

## Why

Python 3.9 is in maintenance mode until October 2025 (see PEP 596 for details), which
means that currently only security patches are released and binary distribution
of Python 3.9 is no longer updated.

## What

The script will build and install Python 3.9 as a framework in the users home directory.

Additional modules are available within the build:

* lzma
* tkinter

## Requirements

The only requirement is relatively recent MacOS (Catalina or newer)
and distribution of developer tools which comes with it - either within Xcode
or CommandLine Developer Tools package. Use `xcode-select --install` to install
all the necessary tools.
